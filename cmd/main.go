package main

import (
	"context"
	"fmt"
	"log"

	"google.golang.org/api/bigquery/v2"
	"google.golang.org/api/option"
)

func main() {
	projectName := ""
	datasetName := ""
	tableName := ""

	ctx := context.Background()
	service, err := bigquery.NewService(ctx, option.WithScopes(bigquery.BigqueryScope))
	if err != nil {
		log.Fatalf("Failed to create BigQuery service client: %v", err)
	}

	call := service.Tables.Get(projectName, datasetName, tableName)
	table, err := call.Do()
	if err != nil {
		log.Fatalf("Failed to retrieve table details: %v", err)
	}

	ddlScript := generateDDLScript(table.Schema.Fields)
	fmt.Println(ddlScript)
}

func generateDDLScript(fields []*bigquery.TableFieldSchema) string {
	ddlScript := "CREATE TABLE `project-id.dataset-id.table-id` (\n"
	for _, field := range fields {
		ddlScript += fmt.Sprintf("  `%s` %s", field.Name, field.Type)

		if field.Mode != "" {
			ddlScript += fmt.Sprintf(" %s", field.Mode)
		}

		if field.DefaultValueExpression != "" {
			ddlScript += fmt.Sprintf(" DEFAULT '%s'", field.DefaultValueExpression)
		}

		ddlScript += ",\n"
	}
	ddlScript = ddlScript[:len(ddlScript)-2] + "\n)" // Remove the trailing comma and newline
	return ddlScript
}
